--drop procedure rptDepartamento;
--drop procedure rptEmpleados;

create procedure rptEmpleados
@sal float
as
select e.codEmpleado,e.nombres,e.apellidos, d.nombreDepto as 'departamento',e.salario, e.edad, u.usuario as 'usuario' from empleado e 
inner join departamento d on d.idDepartamento=e.idDepartamento
inner join usuarios u on u.idusuario=e.idUsuario
where e.salario>=@sal

create procedure rptDepartamento
@cantidadEmpleados int
as
select * from departamento where cantEmpleados >= @cantidadEmpleados;