﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenPractico3.Conexion;

namespace ExamenPractico3
{
    public partial class frmLogin : Form
    {
        public frmLogin()
        {
            InitializeComponent();
        }

        public bool autenticarUser()
        {
            bool existe = false;

            try
            {
                using (exafinalpeEntities db = new exafinalpeEntities())
                {
                    var query = from u in db.usuarios
                                where u.usuario == txtUsuario.Text && u.contra == txtContra.Text
                                select u;

                    if (query.SingleOrDefault() != null)
                    {
                        existe = true;
                    }
                    else
                    {
                        existe = false;
                    }
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocurrió lo siguiente: " + ex.Message);
            }

            return existe;
        }

        private void btnIniciar_Click(object sender, EventArgs e)
        {
            if (!txtUsuario.Text.Equals("") && !txtContra.Text.Equals(""))
            {
                if (autenticarUser() == true)
                {
                    FrmPrincipal principal = new FrmPrincipal();
                    this.Hide();
                    principal.ShowDialog();
                    this.Close();

                }
                else
                {
                    MessageBox.Show("Las credenciales son incorrectas o el usuario no existe", "Mensaje", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show("Los campos no pueden ir vacíos", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnCancelar_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }
    }
}
