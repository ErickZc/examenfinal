﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenPractico3.Conexion;

namespace ExamenPractico3
{
    public partial class frmEmpleados : Form
    {
        bool agregar = false;
        bool modificar = false;

        public frmEmpleados()
        {
            InitializeComponent();
        }

        #region

        public void habilitarBotones()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = true;
            btnModificar.Enabled = true;
            btnAgregar.Enabled = true;
            //btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnModificar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnEliminar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnAgregar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnGuardar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
        }

        public void habilitarCaja(bool i)
        {
            txtIdEmpleado.Enabled = i;
            txtNombres.Enabled = i;
            txtApellidos.Enabled = i;
            txtSalario.Enabled = i;
            txtEdad.Enabled = i;
            cmbDpto.Enabled = i;
            cmbUsuarios.Enabled = i;

            //filtro
            //txtBuscar.Enabled = i;
            //cmbPor.Enabled = i;

        }

        public void recargarDatagrid()
        {
            using (exafinalpeEntities db = new exafinalpeEntities())
            {
                var load = (from empleado in db.empleado join departamento in db.departamento on empleado.idDepartamento equals departamento.idDepartamento join usuarios in db.usuarios on empleado.idUsuario equals usuarios.idusuario select new { empleado.codEmpleado, empleado.nombres, empleado.apellidos, departamento = departamento.nombreDepto, empleado.salario, empleado.edad, usuario = usuarios.usuario }).ToList();
                dgvDatos.DataSource = load;
            }

            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;
            }

        }

        public void llenarCombobox()
        {
            try
            {
                using (exafinalpeEntities db = new exafinalpeEntities())
                {
                    var listaDptos = db.departamento.ToList();
                    if (listaDptos.Count > 0)
                    {
                        cmbDpto.DataSource = listaDptos;
                        cmbDpto.DisplayMember = "nombreDepto";
                        cmbDpto.ValueMember = "idDepartamento";
                        if (cmbDpto.Items.Count >= 1)
                        {
                            cmbDpto.SelectedIndex = -1;
                        }
                    }

                    var listaUsers = db.usuarios.ToList();
                    if (listaUsers.Count > 0)
                    {
                        cmbUsuarios.DataSource = listaUsers;
                        cmbUsuarios.DisplayMember = "usuario";
                        cmbUsuarios.ValueMember = "idUsuario";
                        if (cmbUsuarios.Items.Count >= 1)
                        {
                            cmbUsuarios.SelectedIndex = -1;
                        }
                    }


                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }

        public void llenarDatos()
        {
            // Traslada los datos del registro seleccionado a los campos del formulario
            if (dgvDatos.RowCount > 0)
            {

                txtIdEmpleado.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["codEmpleado"].Value.ToString();
                txtNombres.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["nombres"].Value.ToString();
                txtApellidos.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["apellidos"].Value.ToString();
                txtSalario.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["salario"].Value.ToString();
                txtEdad.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["edad"].Value.ToString();
                cmbDpto.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["departamento"].Value.ToString();
                cmbUsuarios.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["usuario"].Value.ToString();
            }
        }

        public void deshabilitarBotones(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
                btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
                btnModificar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnEliminar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnAgregar.BackColor = ColorTranslator.FromHtml("#CFCFCF");


            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
                btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
                btnModificar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnEliminar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnAgregar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
            }
        }

        public void bloquearFiltro(bool i)
        {
            txtBuscar.Enabled = i;
            cmbPor.Enabled = i;
            btnBuscar.Enabled = i;
            btnRegresar.Enabled = i;
        }

        public void limpiar()
        {
            txtIdEmpleado.Clear();
            txtNombres.Clear();
            txtApellidos.Clear();
            txtEdad.Clear();
            txtSalario.Clear();
            cmbDpto.SelectedIndex = -1;
            cmbUsuarios.SelectedIndex = -1;
            txtBuscar.Clear();
            cmbPor.SelectedIndex = -1;
        }

        public void eliminarRegistro()
        {
            // Elimina el registro seleccionado
            try
            {
                using (exafinalpeEntities db = new exafinalpeEntities())
                {
                    int i = int.Parse(txtIdEmpleado.Text);
                    empleado emp = db.empleado.FirstOrDefault(x => x.codEmpleado == i);
                    db.empleado.Remove(emp);
                    db.SaveChanges();
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }

        public void confirmarEliminación()
        {
            // Envía confirmación al usuario para eliminar el registro
            try
            {
                DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    eliminarRegistro();
                    limpiar();
                    recargarDatagrid();
                    MessageBox.Show("Se ha eliminado correctamente");
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Se ha omitido la eliminacion del registro");
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }

        public void agregarRegistro()
        {
            // Agrega un nuevo registro con los valores ingresados
            try
            {
                using (exafinalpeEntities db = new exafinalpeEntities())
                {

                    if (!cmbDpto.Text.Equals("") && !cmbUsuarios.Text.Equals("") && !txtNombres.Text.Equals("") && !txtApellidos.Text.Equals("") && !txtSalario.Text.Equals("") && !txtEdad.Text.Equals(""))
                    {
                        empleado emp = new empleado();
                        emp.nombres = txtNombres.Text;
                        emp.apellidos = txtApellidos.Text;
                        emp.idDepartamento = int.Parse(cmbDpto.SelectedValue.ToString());
                        emp.edad = int.Parse(txtEdad.Text);
                        emp.salario = double.Parse(txtSalario.Text);
                        emp.idUsuario = int.Parse(cmbUsuarios.SelectedValue.ToString());
                        db.empleado.Add(emp);
                        db.SaveChanges();
                        recargarDatagrid();
                        MessageBox.Show("Se ha agregado el registro correctamente!");
                    }
                    else
                    {
                        MessageBox.Show("Por favor llenar los campos vacios!");
                    }
                }

            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }

        }

        public void modificarRegistro()
        {

            try
            {
                using (exafinalpeEntities db = new exafinalpeEntities())
                {
                    if (!cmbDpto.Text.Equals("") && !cmbUsuarios.Text.Equals("") && !txtNombres.Text.Equals("") && !txtApellidos.Text.Equals("") && !txtSalario.Text.Equals("") && !txtEdad.Text.Equals(""))
                    {

                        int i = int.Parse(txtIdEmpleado.Text);
                        empleado emp = db.empleado.FirstOrDefault(x => x.codEmpleado == i);
                        emp.nombres = txtNombres.Text;
                        emp.apellidos = txtApellidos.Text;
                        emp.idDepartamento = int.Parse(cmbDpto.SelectedValue.ToString());
                        emp.edad = int.Parse(txtEdad.Text);
                        emp.salario = double.Parse(txtSalario.Text);
                        emp.idUsuario = int.Parse(cmbUsuarios.SelectedValue.ToString());
                        db.SaveChanges();
                        recargarDatagrid();
                        MessageBox.Show("Se ha modificado el registro correctamente");
                    }
                    else
                    {
                        MessageBox.Show("Todos los campos deben tener un valor");
                    }
                    limpiar();


                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }

        }

        public void filtrar(string valor, string columna)
        {
            try
            {
                using (exafinalpeEntities db = new exafinalpeEntities())
                {

                    if (!txtBuscar.Text.Equals("") && !cmbPor.Text.Equals(""))
                    {
                        if (cmbPor.Text.Equals("codEmpleado"))
                        {
                            int i = int.Parse(valor);
                            var load = (from d in db.empleado
                                        join t in db.departamento
                                        on d.idDepartamento equals t.idDepartamento
                                        join y in db.usuarios
                                        on d.idUsuario equals y.idusuario
                                        where d.codEmpleado == i
                                        select new { d.codEmpleado, d.nombres, d.apellidos, departamento = t.nombreDepto, d.salario, d.edad, usuario = y.usuario }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }
                        }

                        else if (cmbPor.Text.Equals("nombres"))
                        {
                            var load = (from d in db.empleado
                                        join t in db.departamento
                                        on d.idDepartamento equals t.idDepartamento
                                        join y in db.usuarios
                                        on d.idUsuario equals y.idusuario
                                        where d.nombres.Contains(valor)
                                        select new { d.codEmpleado, d.nombres, d.apellidos, departamento = t.nombreDepto, d.salario, d.edad, usuario = y.usuario }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }
                        }
                        else if (cmbPor.Text.Equals("apellidos"))
                        {
                            var load = (from d in db.empleado
                                        join t in db.departamento
                                        on d.idDepartamento equals t.idDepartamento
                                        join y in db.usuarios
                                        on d.idUsuario equals y.idusuario
                                        where d.apellidos.Contains(valor)
                                        select new { d.codEmpleado, d.nombres, d.apellidos, departamento = t.nombreDepto, d.salario, d.edad, usuario = y.usuario }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }
                        }
                        else if (cmbPor.Text.Equals("departamento"))
                        {
                            var load = (from d in db.empleado
                                        join t in db.departamento
                                        on d.idDepartamento equals t.idDepartamento
                                        join y in db.usuarios
                                        on d.idUsuario equals y.idusuario
                                        where t.nombreDepto.Contains(valor)
                                        select new { d.codEmpleado, d.nombres, d.apellidos, departamento = t.nombreDepto, d.salario, d.edad, usuario = y.usuario }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }
                        }
                        else if (cmbPor.Text.Equals("salario"))
                        {
                            double i = double.Parse(valor);
                            var load = (from d in db.empleado
                                        join t in db.departamento
                                        on d.idDepartamento equals t.idDepartamento
                                        join y in db.usuarios
                                        on d.idUsuario equals y.idusuario
                                        where d.salario == i
                                        select new { d.codEmpleado, d.nombres, d.apellidos, departamento = t.nombreDepto, d.salario, d.edad, usuario = y.usuario }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }
                        }
                        else if (cmbPor.Text.Equals("edad"))
                        {
                            int i = int.Parse(valor);
                            var load = (from d in db.empleado
                                        join t in db.departamento
                                        on d.idDepartamento equals t.idDepartamento
                                        join y in db.usuarios
                                        on d.idUsuario equals y.idusuario
                                        where d.edad == i
                                        select new { d.codEmpleado, d.nombres, d.apellidos, departamento = t.nombreDepto, d.salario, d.edad, usuario = y.usuario }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }
                        }
                        else if (cmbPor.Text.Equals("usuario"))
                        {
                            var load = (from d in db.empleado
                                        join t in db.departamento
                                        on d.idDepartamento equals t.idDepartamento
                                        join y in db.usuarios
                                        on d.idUsuario equals y.idusuario
                                        where y.usuario == valor
                                        select new { d.codEmpleado, d.nombres, d.apellidos, departamento = t.nombreDepto, d.salario, d.edad, usuario = y.usuario }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Por favor complete los campos del filtro");
                    }
                }

            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }

        }

        #endregion

        private void txtEdad_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtSalario_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void frmEmpleados_Load(object sender, EventArgs e)
        {
            habilitarBotones();
            habilitarCaja(false);
            recargarDatagrid();
            llenarCombobox();
            this.Cursor = Cursors.Default;
        }

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            llenarDatos();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            modificar = false;
            deshabilitarBotones("Agregar");
            habilitarCaja(true);
            bloquearFiltro(false);
            txtIdEmpleado.Enabled = false;
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                agregar = false;
                modificar = true;
                habilitarCaja(true);
                deshabilitarBotones("Modificar");
                bloquearFiltro(false);
                txtIdEmpleado.Enabled = false;
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro");
            }​​​​​
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            if (dgvDatos.SelectedRows.Count > 0)
            {
                confirmarEliminación();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro");
            }
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            recargarDatagrid();
            txtBuscar.Clear();
            cmbPor.SelectedIndex = -1;
            limpiar();
        }

        private void btnBuscar_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnRegresar_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                // Valida si la acción que se está realizando es agregar
                if (agregar == true)
                {
                    agregarRegistro();
                    agregar = false;
                    modificar = false;
                }
                else if (modificar == true)
                {
                    modificarRegistro();
                    agregar = false;
                    modificar = false;
                }

                bloquearFiltro(true);
                limpiar();
                habilitarCaja(false);
                habilitarBotones();

            }
            catch (Exception ee)
            {

                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }​​​​​
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            filtrar(txtBuscar.Text, cmbPor.SelectedText);
            limpiar();
        }

        private void gbFiltro_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void frmEmpleados_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }




    }
}
