﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace ExamenPractico3
{
    public partial class FrmPrincipal : Form
    {
        public FrmPrincipal()
        {
            InitializeComponent();
        }

        private void departamentoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            frmDepartamento mdiDepto = new frmDepartamento();
            mdiDepto.MdiParent = this;
            mdiDepto.Show();
            mdiDepto.recargarDatagrid();
        }

        private void acercaDeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAcercaDe mdiAcerca = new frmAcercaDe();
            mdiAcerca.MdiParent = this;
            mdiAcerca.Show(); 
        }

        private void departamentoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmReporteDepartamento mdiReporte = new frmReporteDepartamento();
            mdiReporte.MdiParent = this;
            mdiReporte.Show(); 
        }

        private void empleadoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmReporteEmpleado mdiEmp = new frmReporteEmpleado();
            mdiEmp.MdiParent = this;
            mdiEmp.Show(); 
        }

        private void empleadoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEmpleados mdiEmp = new frmEmpleados();
            mdiEmp.MdiParent = this;
            mdiEmp.Show();
            mdiEmp.recargarDatagrid();
        }

        private void cerrarSesiónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("¿Está seguro que desea cerrar sesión?", "Cerrar Sesión", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.Yes)
            {
                frmLogin login = new frmLogin();
                this.Hide();
                login.ShowDialog();
                this.Close();
            }
        }
    }
}
