//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ExamenPractico3.Conexion
{
    using System;
    using System.Collections.Generic;
    
    public partial class empleado
    {
        public int codEmpleado { get; set; }
        public string nombres { get; set; }
        public string apellidos { get; set; }
        public Nullable<int> idDepartamento { get; set; }
        public Nullable<double> salario { get; set; }
        public Nullable<int> edad { get; set; }
        public Nullable<int> idUsuario { get; set; }
    
        public virtual departamento departamento { get; set; }
        public virtual usuarios usuarios { get; set; }
    }
}
