﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ExamenPractico3.Conexion;

namespace ExamenPractico3
{

    public partial class frmDepartamento : Form
    {
        bool agregar = false;
        bool modificar = false;

        public frmDepartamento()
        {
            InitializeComponent();
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void dgvDatos_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmDepartamento_Load(object sender, EventArgs e)
        {
            habilitarBotones();
            habilitarCaja(false);
            recargarDatagrid();
            llenarCombobox();
            this.Cursor = Cursors.Default;
        }


        #region

        public void bloquearFiltro(bool i) {
            txtBuscar.Enabled = i;
            cmbPor.Enabled = i;
            btnBuscar.Enabled = i;
            btnRegresar.Enabled = i;
        }

        public void filtrar(string valor, string columna)
        {
            try { 
            
                using(exafinalpeEntities db = new exafinalpeEntities()){

                    if (!txtBuscar.Text.Equals("") && !cmbPor.Text.Equals(""))
                    {

                        if (cmbPor.Text.Equals("nombreDepto"))
                        {
                            var load = (from d in db.departamento
                                        join t in db.telefono
                                            on d.codTels equals t.codTels
                                        join e in db.empleado
                                        on d.idDepartamento equals e.idDepartamento
                                        into gj
                                        from x in gj.DefaultIfEmpty()
                                        where d.nombreDepto.Contains(valor)
                                        select new { idDepartamento = d.idDepartamento, nombreDepto = d.nombreDepto, cantEmpleados = d.cantEmpleados, codTels = d.codTels }).ToList();
                            
                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }​​​​​
                        }
                        else if (cmbPor.Text.Equals("idDepartamento"))
                        {
                            int i = int.Parse(valor);
                            var load = (from d in db.departamento
                                        join t in db.telefono
                                            on d.codTels equals t.codTels
                                        join e in db.empleado
                                        on d.idDepartamento equals e.idDepartamento
                                        into gj
                                        from x in gj.DefaultIfEmpty()
                                        where d.idDepartamento == i
                                        select new { idDepartamento = d.idDepartamento, nombreDepto = d.nombreDepto, cantEmpleados = d.cantEmpleados, codTels = d.codTels }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }​​​​​
                        }
                        else if (cmbPor.Text.Equals("cantEmpleados"))
                        {
                            int i = int.Parse(valor);
                            var load = (from d in db.departamento
                                        join t in db.telefono
                                            on d.codTels equals t.codTels
                                        join e in db.empleado
                                        on d.idDepartamento equals e.idDepartamento
                                        into gj
                                        from x in gj.DefaultIfEmpty()
                                        where d.cantEmpleados == i
                                        select new { idDepartamento = d.idDepartamento, nombreDepto = d.nombreDepto, cantEmpleados = d.cantEmpleados, codTels = d.codTels }).ToList();

                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }​​​​​
                        }
                        else if (cmbPor.Text.Equals("codTels"))
                        {
                            int i = int.Parse(valor);
                            var load = (from d in db.departamento
                                        join t in db.telefono
                                            on d.codTels equals t.codTels
                                        join e in db.empleado
                                        on d.idDepartamento equals e.idDepartamento
                                        into gj
                                        from x in gj.DefaultIfEmpty()
                                        where d.codTels == i
                                        select new { idDepartamento = d.idDepartamento, nombreDepto = d.nombreDepto, cantEmpleados = d.cantEmpleados, codTels = d.codTels }).ToList();
                            
                            dgvDatos.DataSource = load;
                            if (dgvDatos.RowCount > 0)
                            {
                                dgvDatos.Rows[0].Selected = false;
                            }​​​​​
                        }
                                                
                    }
                    else {
                        MessageBox.Show("Por favor llene ambas cajas de filtro");
                    }
                }

            }catch(Exception ee){
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }

        }

        public void eliminarRegistro()
        {
            // Elimina el registro seleccionado
            try
            {
                using(exafinalpeEntities db = new exafinalpeEntities()){
                    int i = int.Parse(txtIdDepartamento.Text);
                    departamento depto = db.departamento.FirstOrDefault(x => x.idDepartamento == i);
                    db.departamento.Remove(depto);
                    db.SaveChanges();
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }​​​​​

        public void agregarRegistro()
        {
            // Agrega un nuevo registro con los valores ingresados
            try
               {
                using(exafinalpeEntities db = new exafinalpeEntities()){
                
                    if (!cmbTelefono.Text.Equals("") && !txtCantidad.Text.Equals("") && !txtNombre.Text.Equals(""))
                    {
                        departamento depto = new departamento();
                        //depto.idDepartamento = int.Parse(txtIdDepartamento.Text);
                        depto.cantEmpleados = int.Parse(txtCantidad.Text);
                        depto.nombreDepto = txtNombre.Text;
                        depto.codTels = int.Parse(cmbTelefono.SelectedValue.ToString());
                        db.departamento.Add(depto);
                        db.SaveChanges();
                        recargarDatagrid();
                        MessageBox.Show("Se ha agregado el registro correctamente!");
                    }
                    else
                    {
                        MessageBox.Show("Por favor llenar los campos vacios!");
                    }
                }

            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }

        }

        public void modificarRegistro() {

            try {
                using(exafinalpeEntities db = new exafinalpeEntities()){
                    if (!cmbTelefono.Text.Equals("") && !txtCantidad.Text.Equals("") && !txtNombre.Text.Equals("") && !txtIdDepartamento.Text.Equals(""))
                    {
                        
                        int i = int.Parse(txtIdDepartamento.Text);
                        departamento depto = db.departamento.FirstOrDefault(x => x.idDepartamento == i);
                        depto.nombreDepto = txtNombre.Text;
                        depto.cantEmpleados = int.Parse(txtCantidad.Text);
                        depto.codTels = int.Parse(cmbTelefono.SelectedValue.ToString());
                        db.SaveChanges();
                        recargarDatagrid();
                        MessageBox.Show("Se ha modificado el registro correctamente");
                    }
                    else
                    {
                        MessageBox.Show("Todos los campos deben tener un valor");
                    }
                    limpiar();
                    

                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        
        }

        public void llenarCombobox()
        {
            try
            {
                using(exafinalpeEntities db = new exafinalpeEntities()){
                    var listaTelefono = db.telefono.ToList();
                    if (listaTelefono.Count > 0)
                    {
                        cmbTelefono.DataSource = listaTelefono;
                        cmbTelefono.DisplayMember = "telefono1";
                        cmbTelefono.ValueMember = "codTels";
                        if (cmbTelefono.Items.Count >= 1)
                        {
                            cmbTelefono.SelectedIndex = -1;
                        }
                    }

                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }​​​​​

        public void confirmarEliminación()
        {
            // Envía confirmación al usuario para eliminar el registro
            try
            {
                DialogResult dialogResult = MessageBox.Show("¿Estas seguro que desea eliminar el registro?", "Eliminar Registro", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    eliminarRegistro();
                    recargarDatagrid();
                    MessageBox.Show("Se ha eliminado correctamente");
                }
                else if (dialogResult == DialogResult.No)
                {
                    MessageBox.Show("Se ha omitido la eliminacion del registro");
                }
            }
            catch (Exception ee)
            {
                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }
        }​​​​​

        public void llenarDatos()
        {
            // Traslada los datos del registro seleccionado a los campos del formulario
            if (dgvDatos.RowCount > 0)
            {

                txtIdDepartamento.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["idDepartamento"].Value.ToString();
                txtNombre.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["nombreDepto"].Value.ToString();
                txtCantidad.Text = dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["cantEmpleados"].Value.ToString();
                cmbTelefono.SelectedIndex = int.Parse(dgvDatos.Rows[dgvDatos.CurrentRow.Index].Cells["codTels"].Value.ToString()) - 1;
            }
        }

        public void limpiar()
        {
            // Limpia las cajas de texto
            txtCantidad.Clear();
            txtNombre.Clear();
            txtIdDepartamento.Clear();
            cmbTelefono.SelectedIndex = -1;
            txtBuscar.Clear();
            cmbPor.SelectedIndex = -1;
        }​​​​​

        public void recargarDatagrid() {
            using (exafinalpeEntities db = new exafinalpeEntities())
            {
                var load = (from departamento in db.departamento join telefono in db.telefono on departamento.codTels equals telefono.codTels select new { departamento.idDepartamento, departamento.nombreDepto, departamento.cantEmpleados, departamento.codTels }).ToList();
                dgvDatos.DataSource = load;
            }

            if (dgvDatos.RowCount > 0)
            {
                dgvDatos.Rows[0].Selected = false;
            }​​​​​

        }

        public void habilitarBotones()
        {
            btnGuardar.Enabled = false;
            btnEliminar.Enabled = true;
            btnModificar.Enabled = true;
            btnAgregar.Enabled = true;
            //btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnModificar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnEliminar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnAgregar.BackColor = ColorTranslator.FromHtml("#0275d8");
            btnGuardar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
        }

        public void deshabilitarBotones(string texto)
        {
            if (texto == "Agregar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
                btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
                btnModificar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnEliminar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnAgregar.BackColor = ColorTranslator.FromHtml("#CFCFCF");


            }
            else if (texto == "Modificar")
            {
                btnGuardar.Enabled = true;
                btnEliminar.Enabled = false;
                btnModificar.Enabled = false;
                btnAgregar.Enabled = false;
                btnGuardar.BackColor = ColorTranslator.FromHtml("#0275d8");
                btnModificar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnEliminar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
                btnAgregar.BackColor = ColorTranslator.FromHtml("#CFCFCF");
            }
        }​​​​​

        public void habilitarCaja(bool i)
        {
            txtCantidad.Enabled = i;
            txtIdDepartamento.Enabled = i;
            txtNombre.Enabled = i;
            cmbTelefono.Enabled = i;

            //filtro
            //txtBuscar.Enabled = i;
            //cmbPor.Enabled = i;
            
        }​​​​​

        #endregion

        private void dgvDatos_Click(object sender, EventArgs e)
        {
            llenarDatos();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            agregar = true;
            modificar = false;
            deshabilitarBotones("Agregar");
            habilitarCaja(true);
            bloquearFiltro(false);
            txtIdDepartamento.Enabled = false;
            limpiar();
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            // Valida que se haya seleccionado un registro antes de realizar la modificación
            if (dgvDatos.SelectedRows.Count > 0)
            {
                agregar = false;
                modificar = true;
                habilitarCaja(true);
                deshabilitarBotones("Modificar");
                bloquearFiltro(false);
                txtIdDepartamento.Enabled = false;
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro");
            }​​​​​
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            // Valida que se haya seleccionado un registro antes de realizar la eliminación
            if (dgvDatos.SelectedRows.Count > 0)
            {
                confirmarEliminación();
            }
            else
            {
                MessageBox.Show("Debe seleccionar un registro");
            }​​​​​
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            filtrar(txtBuscar.Text,cmbPor.SelectedText);
            limpiar();
        }

        private void btnRegresar_Click(object sender, EventArgs e)
        {
            recargarDatagrid();
            txtBuscar.Clear();
            cmbPor.SelectedIndex = -1;
        }

        private void btnBuscar_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void frmDepartamento_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void gbFiltro_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Default;
        }

        private void btnRegresar_MouseHover(object sender, EventArgs e)
        {
            this.Cursor = Cursors.Hand;
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            try
                {​​​​​
                // Valida si la acción que se está realizando es agregar
                if (agregar == true)
                {​​​​​
                    agregarRegistro();
                    agregar = false;
                    modificar = false;
                }​​​​​
                else if (modificar == true)
                {​​​​​
                    modificarRegistro();
                    agregar = false;
                    modificar = false;
                }

                bloquearFiltro(true);
                limpiar();
                habilitarCaja(false);
                habilitarBotones();

            }​​​​​catch (Exception ee) {​​​​​

                MessageBox.Show("Ha ocurrido el siguiente error: " + ee.Message);
            }​​​​​
                
        }

        private void txtCantidad_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Limita a que el valor de entrada sea solamente numerico
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {​​​​​
            e.Handled = true;
            }​​​​​
        }

    }
}
